#!/bin/bash
set -e

function register {
  gitlab-runner register --non-interactive "$@"
}

gitlab-runner unregister --all-runners

source /runners.sh

exec gitlab-runner run
