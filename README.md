# GitLab Runner

## Usage

```shell
docker run -v /path/config.toml:/etc/gitlab-runner/config.toml -v /path/runners.sh:/runners.sh gitlab-runner:latest
```

## Environment Variables

All GitLab Runner Environment Variables are supported

### runners.sh

```shell
register \
  --name "my-runner" \
  --tag-list "docker" \
  --other-runner-arguments...

register \
  --name "my-runner-2" \
  --tag-list "ssh" \
  --other-runner-arguments...
```
