FROM gitlab/gitlab-runner:alpine-v14.8.2

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/bin/bash" ]
CMD ["./entrypoint.sh"]
